package pl.batek.restobrag.web;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import pl.batek.restobrag.model.Restaurant;
import pl.batek.restobrag.service.RestaurantService;

@RestController
@Validated
@RequestMapping(value="/restaurants",
		 consumes = MediaType.APPLICATION_JSON_VALUE,
         produces = MediaType.APPLICATION_JSON_VALUE)
public class MainController {
	
	private final static String NOT_PROVIDED ="";
	//TODO Move to a properties file
	private final static int MAX_TYPE_LENGTH = 100;
	private final static long MIN_LATITUDE = -90;
	private final static long MAX_LATITUDE = 90;
	private final static long MIN_LONGITUDE = -180;
	private final static long MAX_LONGITUDE = 180;
	
	private RestaurantService service;
	
	@Autowired
	public MainController(RestaurantService service) {
		this.service = service;
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Restaurant addRestaurant(@RequestBody Restaurant restaurant) {
		return this.service.addRestaurant(restaurant);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	public List<Restaurant> getRestaurants( 
						@RequestParam("latitude") @Min(MIN_LATITUDE) @Max(MAX_LATITUDE) 
						double latitude, 
						@RequestParam("longitude") @Min(MIN_LONGITUDE) @Max(MAX_LONGITUDE) 
						double longitude, 
						@RequestParam(value="foodType", defaultValue=NOT_PROVIDED) 
						@Size(max=MAX_TYPE_LENGTH) 
						String foodType) {
		
		if(NOT_PROVIDED.equals(foodType)) {
			return this.service.getRestaurantsSortedByDistance(latitude, longitude);
		}
		else {
			return this.service.getRestaurantsSortedByDistanceAndType(latitude, longitude, foodType);
		}
	}

}
