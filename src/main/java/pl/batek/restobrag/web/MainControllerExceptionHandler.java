package pl.batek.restobrag.web;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class MainControllerExceptionHandler {
	private Logger logger = LoggerFactory.getLogger(MainControllerExceptionHandler.class);
	
	@ExceptionHandler(ConstraintViolationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public String handleValidationErrors(ConstraintViolationException e) {
		Set<ConstraintViolation<?>> violations = e.getConstraintViolations();
	    StringBuilder strBuilder = new StringBuilder();
	    for(ConstraintViolation<?> v : violations ) {
	    	strBuilder.append(v.getMessage() + "\n");
	    }
	    logger.error(strBuilder.toString());  //TODO implement better logging; check if it's asynchronous
	    return strBuilder.toString();
	}
}
