package pl.batek.restobrag.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Parameter;

@Entity
//TODO Add validation annotations
public class Restaurant {
//	@GenericGenerator(
//			name = "ID_GENERATOR",
//			strategy = "enhanced-sequence",
//			parameters = {
//					@Parameter(
//							name = "sequence_name",
//							value = "DEFAULT_SEQUENCE"
//							),
//					@Parameter(
//							name = "initial_value",
//							value = "1000"
//							)			
//			}
//	)
//	@GeneratedValue(generator = "ID_GENERATOR")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Id
	@Column(name="id")
	private long id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="foodType")
	private String foodType;
	
	@Column(name="latitude")
	private double latitude;
	
	@Column(name="longitude")
	private double longitude;

	public Restaurant() {
	}
	
	public Restaurant(String name, String foodType, double latitude, double longitude) {
		this.name = name;
		this.foodType = foodType;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public Restaurant(long id, String name, String foodType, double latitude, double longitude) {
		this.id = id;
		this.name = name;
		this.foodType = foodType;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFoodType() {
		return foodType;
	}
	public void setFoodType(String foodType) {
		this.foodType = foodType;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}	
}
