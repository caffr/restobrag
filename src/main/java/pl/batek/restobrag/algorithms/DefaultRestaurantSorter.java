package pl.batek.restobrag.algorithms;

import static pl.batek.restobrag.algorithms.DistanceCalculator.calculateDistance;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.stereotype.Component;

import pl.batek.restobrag.model.Restaurant;

public class DefaultRestaurantSorter implements RestaurantSorter {

	@Override
	public List<Restaurant> sortByDistanceAndType(
			List<Restaurant> restaurants, String foodType, double latitude, double longitude) {
		return sortByType(
				presortByDistance(restaurants.stream(), latitude, longitude), 
				foodType);
	}
	
	@Override
	public List<Restaurant> sortByDistance(
			List<Restaurant> restaurants, double latitude, double longitude) {
		return presortByDistance( restaurants.stream(), latitude, longitude ).
																collect(Collectors.toList());
	}


	private Stream<Restaurant> presortByDistance(Stream<Restaurant> restaurants, double latitude, double longitude) {
		return restaurants.sorted( 
				(r1,r2) -> Double.compare(
								calculateDistance(r1.getLatitude(), r1.getLongitude(), latitude, longitude),
								calculateDistance(r2.getLatitude(), r2.getLongitude(), latitude, longitude) ) );
	}
	
	/** Returns list of Restaurants. The ones with specified food type are first group. 
	 * Second group are the ones with different food types. Sorting by distance within groups remains.
	 * @param restaurants Stream of Restaurants pre-sorted by distance from a given point.
	 * @param foodType Desired food type to sort by.
	 * @return List of Restaurants sorted by food type. 
	 */
	private List<Restaurant> sortByType(Stream<Restaurant> restaurants, String foodType) {
		// TODO Check if ordering of the stream will be kept while partitioning. 
		// split a stream into two lists, one for specified foodType (key=TRUE), second for all other foodTypes (key=FALSE)
		Map<Boolean,List<Restaurant>> splitGroups = restaurants.collect(
														Collectors.partitioningBy(
																r -> foodType.equals( r.getFoodType() ) ) );
		List<Restaurant> list = splitGroups.get(Boolean.TRUE); //get list of restaurants with specified foodType
		list.addAll(splitGroups.get(Boolean.FALSE)); //append list of other restaurants
		return list;
	}
}
