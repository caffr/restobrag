package pl.batek.restobrag.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.batek.restobrag.algorithms.RestaurantSorter;
import pl.batek.restobrag.data.RestaurantRepository;
import pl.batek.restobrag.model.Restaurant;

@Service
public class DefaultRestaurantService implements RestaurantService {
	
	private RestaurantRepository repository;
	private RestaurantSorter sorter;
	
	private RestaurantRepository getRepository() {
		return repository;
	}
	@Autowired
	public void setRepository(RestaurantRepository repository) {
		this.repository = repository;
	}

	private RestaurantSorter getSorter() {
		return sorter;
	}
	@Autowired
	public void setSorter(RestaurantSorter sorter) {
		this.sorter = sorter;
	}

	@Override
	public List<Restaurant> getRestaurantsSortedByDistance(double latitude, double longitude) {
		return sorter.sortByDistance(
				repository.getAllRestaurants(), latitude, longitude);
	}
	
	@Override
	public List<Restaurant> getRestaurantsSortedByDistanceAndType(double latitude, double longitude, String foodType) {
		return sorter.sortByDistanceAndType(
				repository.getAllRestaurants(), foodType, latitude, longitude);
	}
	
	@Override
	public Restaurant addRestaurant(Restaurant restaurant) {
		return this.repository.addRestaurant(restaurant);
	}

	
}
