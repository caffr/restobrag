package pl.batek.restobrag.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.batek.restobrag.data.RestaurantRepository;
import pl.batek.restobrag.model.Restaurant;


public interface RestaurantService {
		
	List<Restaurant> getRestaurantsSortedByDistance(double latitude, double longitude);
	
	List<Restaurant> getRestaurantsSortedByDistanceAndType(double latitude, double longitude, String foodType);
	
	Restaurant addRestaurant(Restaurant r);
}
