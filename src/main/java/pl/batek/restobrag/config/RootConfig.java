package pl.batek.restobrag.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.ImportResource;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import pl.batek.restobrag.algorithms.DefaultRestaurantSorter;
import pl.batek.restobrag.algorithms.RestaurantSorter;
import pl.batek.restobrag.service.DefaultRestaurantService;
import pl.batek.restobrag.service.RestaurantService;

@Configuration
@ComponentScan(basePackages = { "pl.batek.restobrag" }, excludeFilters = {
		@Filter(type = FilterType.ANNOTATION, value = EnableWebMvc.class) })
public class RootConfig {
	@Bean
	public MethodValidationPostProcessor methodValidationPostProcessor() {
		return new MethodValidationPostProcessor();
	}
	
	//	PersistenceExceptionTranslationPostProcessor is a bean post-processor that adds
	//	an adviser to any bean that’s annotated with @Repository so that any platform-specific
	//	exceptions are caught and then rethrown as one of Spring’s unchecked data-access
	//	exceptions.
	@Bean
	public BeanPostProcessor persistenceTranslation() {
		return new PersistenceExceptionTranslationPostProcessor();
	}

	@Bean
	public RestaurantSorter restaurantSorter() {
		return new DefaultRestaurantSorter();
	}
}
