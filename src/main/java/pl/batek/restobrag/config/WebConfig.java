package pl.batek.restobrag.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import pl.batek.restobrag.algorithms.DefaultRestaurantSorter;
import pl.batek.restobrag.algorithms.RestaurantSorter;
import pl.batek.restobrag.data.RestaurantRepository;
import pl.batek.restobrag.service.DefaultRestaurantService;
import pl.batek.restobrag.service.RestaurantService;

@Configuration
@EnableWebMvc
@ComponentScan("pl.batek.restobrag.web")
public class WebConfig extends WebMvcConfigurerAdapter {
	
	/** Set default servlet handler, this is the same as <mvc:default-servlet-handler/> **/
	@Override
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		configurer.enable();
	}
	
	
}
