package pl.batek.restobrag.config;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;

import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
public class DatabaseConfig {
	
	//TODO Move those strings to external property file
	@Bean
	public LocalSessionFactoryBean sessionFactory(DataSource dataSource) {
		LocalSessionFactoryBean sfb = new LocalSessionFactoryBean();
		sfb.setDataSource(dataSource);
		sfb.setPackagesToScan(new String[] { "pl.batek.restobrag.model" });
		Properties props = new Properties();
		props.setProperty("dialect", "org.hsqldb.jdbcDriver");
		props.setProperty("show_sql", "true");
		props.setProperty("hbm2dll.auto", "create"); //TODO Change that to update
		sfb.setHibernateProperties(props);
		return sfb;
	}
	
	
	@Bean
	public DataSource dataSource() {
		// no need shutdown, EmbeddedDatabaseFactoryBean will take care of this
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		EmbeddedDatabase db = builder
			.setType(EmbeddedDatabaseType.HSQL)
			.addScript("create-db.sql")
			.build();
		
		return db;
	}

	/*
	 TODO Remove later
	DataSource dataSource() {
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		EmbeddedDatabase db = builder.setType(EmbeddedDatabaseType.HSQL).build();
		return db;
//TODO remove those comments
//		String url = "";
//		Properties props = new Properties();
//		props.setProperty("driverClassName", "org.hsqldb.jdbcDriver");
//		props.setProperty("username", "sa");
//		props.setProperty("password", "");
//		DataSource dataSource = new DriverManagerDataSource(url, props);
//		return dataSource;
	}
	*/
}
