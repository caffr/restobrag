package pl.batek.restobrag.data;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.stereotype.Repository;

import pl.batek.restobrag.model.Restaurant;

@Repository
public class HibernateRestaurantRepository implements RestaurantRepository {
	
	private LocalSessionFactoryBean sessionFactoryBean;
	private SessionFactory sessionFactory;
	
	@Autowired
	public HibernateRestaurantRepository(LocalSessionFactoryBean sessionFactoryBean) {
		this.sessionFactoryBean = sessionFactoryBean;
		this.sessionFactory = sessionFactoryBean.getConfiguration().buildSessionFactory();
		
	}
	
	public List<Restaurant> getAllRestaurants() {
		Session session = this.sessionFactory.openSession();
		List<Restaurant> restaurants = session.createCriteria(Restaurant.class).list();
		session.close(); //TODO move it to finally block
		
		return restaurants;
	}
	
	public Restaurant addRestaurant(Restaurant restaurant) {
		Session session = this.sessionFactory.openSession();
		session.save(restaurant);
		session.close(); //TODO move it to finally block
		
		return restaurant;
	}
}
