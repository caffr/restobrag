package pl.batek.restobrag.config;

import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;

import pl.batek.restobrag.service.RestaurantService;

@Configuration
public class TestConfig {
	@Bean
	public RestaurantService restaurantService() {
		return Mockito.mock(RestaurantService.class);
	}
	
	@Bean
	public MethodValidationPostProcessor methodValidationPostProcessor() {
		return new MethodValidationPostProcessor();
	}
}
